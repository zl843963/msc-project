This research is focused on building FL-based IDS.
There are 4 folders in this repository. Each one represents the data samples described in the paper. Data sample 3 was chosen as the baseline data sample. In the folder there will be several main files:

- initial model (It trains the initial model and saves it as a pickle file. Moreover, at the bottom of the file, the code for data sample preparation can be found. This code creates local datasets for each client "client1_data.pkl - "client20_data.pkl"). Each data file represents the subset of the client-side dataset. In the initial model, the number of clients that will split the initial dataset can be changed from 20 to any number needed for research purposes.

-server_datasample3.ipynb - this is our main server. 
The following parameters can be altered to test the system performance under different set-ups:
round_num = 3
num_clients = 20

- client.ipynb (client_1 - client_20) - Client device - which needs to be activated only after the server is initialized. 
On the client side "num_epoch  =  5" - can be alternated for experimenting with parameters.

- In the "installation requirements" file necessary installation is mentioned for the system to run the code. If the system is not working, I recommend running it in Virtual Environment.